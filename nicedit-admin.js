
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    function check_buttons() {
      if ($('#edit-nicedit-fullpanel').is(':checked')) {
        $('#nicedit-settings-form .form-checkboxes input').attr('disabled', 'disabled');
      }
      else {
        $('#nicedit-settings-form .form-checkboxes input').removeAttr('disabled');
      }
    }

    $('#edit-nicedit-fullpanel').click(function () {
      check_buttons();
    });

    check_buttons();
  }); 
}
