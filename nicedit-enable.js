
if (Drupal.jsEnabled) {
  $(document).ready(function () {
    $('.nicedit-disable').html('<a href="javascript:">show html</a>').click(function() {
      textarea = $(this).parent().find('textarea');
      if ($(textarea).is(':hidden')) {
        textarea.show();
        textarea.parent().find('div').hide();
        $(this).find('a').html('show editor');
      }
      else {
        textarea.hide();
        textarea.parent().find('div').show();
        $(this).find('a').html('show html');
      }
      return false;
    });
  }); 
}
