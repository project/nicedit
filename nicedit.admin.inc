<?php

/**
 * nicedit.admin.inc
 *
 * This page is only loaded when doing admin stuff.
 * Which minimizes the number of lines that get parsed on most pages.
 */

/**
 * Implement admin/settings/nicedit.
 */
function nicedit_settings_form() {
  $nesid = $_REQUEST['nesid'];
  if (isset($nesid)) {
    if ($nesid <= 0) {
      drupal_set_message(t('Creating a new configuration'));
    }
    else {
      $setting = _nicedit_load_setting($nesid);
      drupal_set_message(t('Configuring settings for !name', array('!name' => ucfirst($setting->name))));
    }
  }
  else {
    $setting = _nicedit_load_setting('default');
  }
  $form = _nicedit_settings_form($setting, isset($nesid) && $nesid == 0);
  $form['#submit']['nicedit_settings_form_submit'] = array();
  return system_settings_form($form);
}

function _nicedit_settings_form($setting = NULL, $new = FALSE) {
  drupal_add_js(drupal_get_path('module', 'nicedit') .'/nicedit-admin.js');
  $form = array();
  if ($new || (isset($setting) && $setting->name != 'default')) {
    $form['nicedit_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => isset($setting->name) ? $setting->name : '',
      '#description' => t('Name the configuration, used when selecting which configuration to use.'),
      '#required' => TRUE,
    );
  }
  $form['nicedit_fullpanel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Full Panel'),
    '#default_value' => isset($setting->fullpanel) ? $setting->fullpanel : 0,
    '#description' => t('Selecting this, selects all the other buttons'),
  );
  $buttons = _nicedit_buttons();
  // NOTE: this index can't be 'buttons' because that conflicts with the settings form buttons
  $form['nicedit_buttons'] = array(
    '#type' => 'checkboxes',
    '#options' => $buttons,
    '#default_value' => isset($setting->buttons) ? $setting->buttons : array_keys($buttons),
    '#theme' => 'nicedit_settings_form_buttons',
    '#description' => t('Select individual buttons to display'),
  );
  if (isset($setting->nesid)) {
    $form['nesid'] = array(
      '#type' => 'hidden',
      '#value' => $setting->nesid,
    );
  }
  if (isset($_REQUEST['name'])) {
    $form['name'] = array(
      '#type' => 'hidden',
      '#value' => $_REQUEST['name'],
    );
  }
  if (isset($_REQUEST['page'])) {
    $form['page'] = array(
      '#type' => 'hidden',
      '#value' => $_REQUEST['page'],
    );
  }
  return $form;
}

function nicedit_settings_form_submit($form_id, &$form) {
  $nesid = $form['nesid'];
  if ($form['op'] == t('Save configuration')) {
    $fullpanel = $form['nicedit_fullpanel'];
    $buttons = serialize($form['nicedit_buttons']);
    if (isset($nesid) && $nesid > 0) {
      db_query("UPDATE {nicedit_settings} SET fullpanel = %d, buttons = '%s' WHERE nesid = %d", $fullpanel, $buttons, $nesid);
    }
    else {
      $name = isset($form['nicedit_name']) ? $form['nicedit_name'] : 'default';
      $nesid = db_next_id('nicedit_nesid');
      db_query("INSERT INTO {nicedit_settings} (fullpanel, buttons, nesid, name) VALUES (%d, '%s', %d, '%s')", $fullpanel, $buttons, $nesid, $name);
    }
  }
  elseif ($form['op'] == t('Reset to defaults') && isset($nesid) && $nesid > 0) {
    db_query("DELETE FROM {nicedit_settings} WHERE nesid = %d", $nesid);
    drupal_set_message(t('The configuration options have been reset to their default values.'));
  }
  if (isset($form['page'])) {
    if (isset($form['name'])) {
      $form['nesid'] = $nesid;
      _nicedit_config_form_submit_save($form);
    }
    drupal_goto($form['page']);
  }
}

function theme_nicedit_settings_form_buttons($form) {
  $rows = array();
  foreach ($form as $element_id => $element) {
    if ($element_id[0] != '#') {
      if (!isset($cols)) {
        $cols = array();
      }
      $cols[] = drupal_render($element);
      if (count($cols) == 3) {
        $rows[] = $cols;
        unset($cols);
      }
    }
  }
  if (isset($cols)) {
    $rows[] = $cols;
  }
  return theme('table', array(), $rows);
}

/**
 * Implement admin/settings/config.
 */
function nicedit_config_form() {
  // get the arguments
  $page = $_REQUEST['page'];
  $name = $_REQUEST['name'];
  if ($page && $name) {
    drupal_set_message(t('Configuring field %name on page %page.', array('%name' => $name, '%page' => $page)));
  }
  else {
    drupal_set_message(t('The field or page name were lost.'), 'error');
    drupal_goto('admin/settings/nicedit');
  }

  // load the existing settings configurations
  $results = db_query("SELECT * FROM {nicedit_settings} ORDER BY nesid ASC");
  $settings = array();
  while ($setting = db_fetch_object($results)) {
    if ($setting->name == 'default') {
      $default_nesid = $setting->nesid;
    }
    $settings[$setting->nesid] = ucfirst($setting->name);
  }
  if (count($settings) == 0) {
    $settings[-1] = 'default';
  }
  if ($page && $name) {
    if ($config_nesid = db_result(db_query("SELECT nesid FROM {nicedit_config} WHERE page = '%s' AND name = '%s'", $page, $name))) {
      $nesid = $config_nesid;
    }
  }
  if (!isset($nesid) && isset($default_nesid)) {
    $nesid = $default_nesid;
  }

  // Let the user select an existing settings configuration
  $form = array();
  $form['nesid'] = array(
    '#type' => 'select',
    '#options' => $settings,
    '#default_value' => $nesid,
    '#title' => t('Settings configuration'),
    '#description' => t('Select a settings configuration to use with this field.  Try to re-use existing settings configurations whenever possible.'),
  );

  // save the page and name
  $form['page'] = array(
    '#type' => 'hidden',
    '#value' => $page,
  );
  $form['name'] = array(
    '#type' => 'hidden',
    '#value' => $name,
  );

  // Add the buttons
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  $form['edit'] = array(
    '#type' => 'submit',
    '#value' => t('Edit settings'),
  );
  $form['new'] = array(
    '#type' => 'submit',
    '#value' => t('Create new settings'),
  );
  $form['disable'] = array(
    '#type' => 'submit',
    '#value' => t('Disable editor on this field'),
  );
  return $form;
}

function nicedit_config_form_submit($form_id, &$form) {
  if ($form['op'] == t('Save configuration')) {
    _nicedit_config_form_submit_save($form, TRUE);
    drupal_goto($form['page']);
  }
  elseif ($form['op'] == t('Edit settings')) {
    _nicedit_config_form_submit_save($form);
    $query = array();
    $query[] = "nesid=". $form['nesid'];
    $query[] = "page=". $form['page'];
    $query[] = "name=". $form['name'];
    drupal_goto('admin/settings/nicedit', implode('&', $query));
  }
  elseif ($form['op'] == t('Disable editor on this field')) {
    $query = array();
    $query[] = "name=". $form['name'];
    $query[] = "page=". $form['page'];
    $query[] = "enable=0";
    drupal_goto('admin/settings/nicedit/enable', implode('&', $query));
  }
  elseif ($form['op'] == t('Create new settings')) {
    $query = array();
    $query[] = "nesid=0";
    $query[] = "page=". $form['page'];
    $query[] = "name=". $form['name'];
    drupal_goto('admin/settings/nicedit', implode('&', $query));
  }
}

function _nicedit_config_form_submit_save(&$form, $message = FALSE) {
  $page = $form['page'];
  $name = $form['name'];
  $default_nesid = db_result(db_query("SELECT nesid FROM {nicedit_settings} WHERE name = 'default'"));
  if ($form['nesid'] == $default_nesid) {
    db_query("DELETE FROM {nicedit_config} WHERE page = '%s' AND name = '%s'", $page, $name);
  }
  elseif (db_result(db_query("SELECT page FROM {nicedit_config} WHERE page = '%s' AND name = '%s'", $page, $name))) {
    db_query("UPDATE {nicedit_config} SET nesid = %d WHERE page = '%s' AND name = '%s'", $form['nesid'], $page, $name);
  }
  else {
    db_query("INSERT INTO {nicedit_config} (nesid, page, name, disabled) VALUES (%d, '%s', '%s', 0)", $form['nesid'], $page, $name);
  }
  if ($message) {
    drupal_set_message(t('The configuration options have been saved.'));
  }
}

/**
 * Implement admin/settings/nicedit/enable.
 */
function nicedit_settings_enable_page() {
  $page = $_REQUEST['page'];
  $name = $_REQUEST['name'];
  $disabled = isset($_REQUEST['enable']) ? ($_REQUEST['enable'] == 1 ? 0 : 1) : 0;
  $config = db_fetch_object(db_query("SELECT page, nesid FROM {nicedit_config} WHERE page = '%s' AND name = '%s'", $page, $name));
  if (isset($config->page)) {
    if ($disabled || isset($config->nesid)) {
      db_query("UPDATE {nicedit_config} SET disabled = %d WHERE page = '%s' AND name = '%s'", $disabled, $page, $name);
    }
    else {
      db_query("DELETE FROM {nicedit_config} WHERE page = '%s' AND name = '%s'", $page, $name);
    }
  }
  elseif ($disabled) {
    db_query("INSERT INTO {nicedit_config} (page, name, disabled) VALUES ('%s', '%s', %d)", $page, $name, $disabled);
  }
  drupal_goto($page);
}

